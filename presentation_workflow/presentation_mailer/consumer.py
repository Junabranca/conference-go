import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
import time
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            contact_info = json.loads(body)
            presenter_name = contact_info["presenter_name"]
            email_address = contact_info["presenter_email"]
            title = contact_info["title"]
            send_mail(
                "Your presentation has been accepted",
                f"{presenter_name} we are happy to tell you that your presentation {title} has been accepted!",
                "admin@conference.go",
                [email_address],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            contact_info = json.loads(body)
            presenter_name = contact_info["presenter_name"]
            email_address = contact_info["presenter_email"]
            title = contact_info["title"]
            send_mail(
                "Your presentation has been rejected",
                f"{presenter_name} we are sorry to tell you that your presentation {title} has been rejected.",
                "admin@conference.go",
                [email_address],
                fail_silently=False,
            )

        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="presentation_approval")
            channel.basic_consume(
                queue="presentation_approval",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.queue_declare(queue="presentation_rejection")
            channel.basic_consume(
                queue="presentation_rejection",
                on_message_callback=process_rejection,
                auto_ack=True,
            )
            channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
