from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_pexel(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_openweather(city, state):
    us_iso_code = "ISO 3166-2:US"
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{us_iso_code}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response_geo = requests.get(geo_url)
    geocode = json.loads(response_geo.content)
    lat = geocode[0]["lat"]
    lon = geocode[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    weather_data = json.loads(weather_response.content)
    temperature = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    return {
        "temperature": temperature,
        "description": description,
    }
