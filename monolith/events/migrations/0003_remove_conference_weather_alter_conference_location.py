# Generated by Django 4.0.3 on 2023-02-14 23:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_conference_weather_location_picture_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conference',
            name='weather',
        ),
        migrations.AlterField(
            model_name='conference',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='conference', to='events.location'),
        ),
    ]
